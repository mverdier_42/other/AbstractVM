/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Parser.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 19:04:44 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/21 18:20:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>
#include <vector>

#include "tokensStruct.hpp"
#include "IOperand.hpp"
#include "OpFactory.hpp"
#include "MutantStack.hpp"

class Parser {
	public:
		Parser(MutantStack<const IOperand*> &opStack);
		Parser(Parser const & src);
		virtual ~Parser();

		Parser& operator=(Parser const& rhs);

		void						setTokens(std::vector<tokensType> &tokens);
		std::vector<std::string>	&getExceptions();
		void						parse();

		class TooSmallStackException : public std::exception {
			public:
				TooSmallStackException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~TooSmallStackException() throw();

			private:
				std::string		_error;
		};

		class AssertException : public std::exception {
			public:
				AssertException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~AssertException() throw();

			private:
				std::string		_error;
		};

		class DivException : public std::exception {
			public:
				DivException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~DivException() throw();

			private:
				std::string		_error;
		};

	private:
		Parser();

		void							opPush(tokensType &token);
		void							opPop(tokensType &token);
		void							opDump(tokensType &token);
		void							opAssert(tokensType &token);
		void							opAdd(tokensType &token);
		void							opSub(tokensType &token);
		void							opMul(tokensType &token);
		void							opDiv(tokensType &token);
		void							opMod(tokensType &token);
		void							opPrint(tokensType &token);
		void							opExit(tokensType &token);
		eOperandType					getType(tokensType &token) const;

		MutantStack<const IOperand*>	&_opStack;
		OpFactory						_opFactory;
		typedef void (Parser::*FuncType)(tokensType &);
		std::vector<FuncType>			_ops;
		std::vector<tokensType>			_tokens;
		std::vector<std::string>		_exceptions;
		static const std::string		_instructions[];
		static const std::string		_types[];
};

#endif
