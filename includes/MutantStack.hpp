/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MutantStack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/21 17:04:59 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/21 17:41:48 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

#include <stack>

template<typename T>
class MutantStack : public std::stack<T> {
	private:
		typedef std::stack<T>	_stack;
		typedef typename std::stack<T>::container_type::iterator	iterator;

	public:
		MutantStack() {}
		MutantStack(const MutantStack &rhs) : _stack(rhs) {}
		using	_stack::operator=;
		~MutantStack() {}

		iterator	begin() { return _stack::c.begin(); }
		iterator	end() { return _stack::c.end(); }
};

#endif
