/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokensStruct.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 19:09:37 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/12 18:09:43 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOKENSSTRUCT_HPP
#define TOKENSSTRUCT_HPP

#include <string>

typedef struct				tokensStruct {
	std::string				instruction;
	std::string				type;
	std::string				value;
	std::string				line;
}							tokensType;

#endif
