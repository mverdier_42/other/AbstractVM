/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Operand.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/29 17:20:22 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/22 18:12:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPERAND_HPP
#define OPERAND_HPP

#include <iostream>
#include <cstdint>
#include <cmath>
#include <sstream>
#include <limits>
#include <exception>

#include "IOperand.hpp"
#include "OpFactory.hpp"

template<class T>
class Operand : public IOperand {
	public:
		Operand(const std::string &value, eOperandType type) : _value(std::strtof(value.c_str(), NULL)), _type(type) {
			std::ostringstream	os;

			os << static_cast<double>(_value);
			_str = os.str();
		}
		Operand(Operand const & src) { *this = src; }
		virtual ~Operand() {}

		Operand& 			operator=(Operand const& rhs) {
			_value = static_cast<T>(rhs._value);
			_type = rhs._type;
			_str = rhs._str;
			return (*this);
		}

		int					getPrecision() const { return (_type); }
		eOperandType		getType() const { return (_type); }
		T					getValue() const { return (_value); }
		
		const IOperand* 	operator+(const IOperand& rhs) const {
			long double		result = _value + getValue(rhs);

			return (resultOperand(rhs, result));
		}

		const IOperand* 	operator-(const IOperand& rhs) const {
			long double		result = _value - getValue(rhs);

			return (resultOperand(rhs, result));
		}

		const IOperand* 	operator*(const IOperand& rhs) const {
			long double		result = _value * getValue(rhs);

			return (resultOperand(rhs, result));
		}

		const IOperand* 	operator/(const IOperand& rhs) const {
			long double		val2 = getValue(rhs);
			long double		result;
			
			if (val2 == 0)
				throw DivException("Error : Division by 0");

			result = _value / val2;

			return (resultOperand(rhs, result));
		}

		const IOperand* 	operator%(const IOperand& rhs) const {
			long double		val2 = getValue(rhs);
			long double		result;
			
			if (val2 == 0)
				throw ModException("Error : Modulo by 0");

			if (_type >= FLOAT || rhs.getType() >= FLOAT)
				result = 0;	// Floats numbers do not have remainder.
			else
				result = fmod(_value, val2);

			return (resultOperand(rhs, result));
		}

		class ResultOverflowException : public std::exception {
			public:
				ResultOverflowException(std::string error) throw() { _error = error; }
				virtual const char* what() const throw() { return (_error.c_str()); }
				virtual ~ResultOverflowException() throw() {}

			private:
				std::string		_error;
		};

		class DivException : public std::exception {
			public:
				DivException(std::string error) throw() { _error = error; }
				virtual const char *what() const throw() { return (_error.c_str()); }
				virtual ~DivException() throw() {}

			private:
				std::string		_error;
		};

		class ModException : public std::exception {
			public:
				ModException(std::string error) throw() { _error = error; }
				virtual const char *what() const throw() { return (_error.c_str()); }
				virtual ~ModException() throw() {}

			private:
				std::string		_error;
		};

		const std::string&	toString() const { return (_str); }

	private:
		Operand();

		long double			getValue(const IOperand& rhs) const {
			if (rhs.getType() == INT8) {
				const Operand<int8_t>	*op = dynamic_cast<const Operand<int8_t>* >(&rhs);
				return (op->getValue());
			}
			if (rhs.getType() == INT16) {
				const Operand<int16_t>	*op = dynamic_cast<const Operand<int16_t>* >(&rhs);
				return (op->getValue());
			}
			if (rhs.getType() == INT32) {
				const Operand<int32_t>	*op = dynamic_cast<const Operand<int32_t>* >(&rhs);
				return (op->getValue());
			}
			if (rhs.getType() == FLOAT) {
				const Operand<float>	*op = dynamic_cast<const Operand<float>* >(&rhs);
				return (op->getValue());
			}
			else {
				const Operand<double>	*op = dynamic_cast<const Operand<double>* >(&rhs);
				return (op->getValue());
			}
		}

		void				checkOverflows(long double result, eOperandType type) const {
			if (type == INT8) {
				if (result > std::numeric_limits<int8_t>::max())
					throw ResultOverflowException("Error : Result overflows");
				if (result < std::numeric_limits<int8_t>::min())
					throw ResultOverflowException("Error : Result underflows");
			}

			if (type == INT16) {
				if (result > std::numeric_limits<int16_t>::max())
					throw ResultOverflowException("Error : Result overflows");
				if (result < std::numeric_limits<int16_t>::min())
					throw ResultOverflowException("Error : Result underflows");
			}

			if (type == INT32) {
				if (result > std::numeric_limits<int32_t>::max())
					throw ResultOverflowException("Error : Result overflows");
				if (result < std::numeric_limits<int32_t>::min())
					throw ResultOverflowException("Error : Result underflows");
			}

			if (type == FLOAT) {
				if (result > std::numeric_limits<float>::max() && result != std::numeric_limits<float>::infinity())
					throw ResultOverflowException("Error : Result overflows");
				if (result < -std::numeric_limits<float>::max() && result != -std::numeric_limits<float>::infinity())
					throw ResultOverflowException("Error : Result underflows");
			}

			if (type == DOUBLE) {
				if (result > std::numeric_limits<double>::max() && result != std::numeric_limits<double>::infinity())
					throw ResultOverflowException("Error : Result overflows");
				if (result < -std::numeric_limits<double>::max() && result != -std::numeric_limits<double>::infinity())
					throw ResultOverflowException("Error : Result underflows");
			}
		}

		const IOperand*		resultOperand(const IOperand& rhs, long double result) const {
			eOperandType	type;

			type = (getType() >= rhs.getType() ? getType() : rhs.getType());

			checkOverflows(result, type);
			return (_opF.createOperand(type, std::to_string(result)));
		}

		T					_value;
		eOperandType		_type;
		OpFactory			_opF;
		std::string			_str;
};

#endif
