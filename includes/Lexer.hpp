/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Lexer.hpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 19:19:45 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/21 18:22:44 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEXER_HPP
#define LEXER_HPP

#include <iostream>
#include <string>
#include <vector>
#include <exception>

#include "tokensStruct.hpp"

class Lexer {
	public:
		Lexer(std::istream &stream);
		Lexer(Lexer const & src);
		virtual ~Lexer();

		Lexer& operator=(Lexer const& rhs);

		void						setStream(std::istream &stream);
		std::vector<tokensType>		&getTokens();
		std::vector<std::string>	&getExceptions();

		void						tokenise();

		class BadInstructionException : public std::exception {
			public:
				BadInstructionException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~BadInstructionException() throw();

			private:
				std::string		_error;
		};

		class BadTypeException : public std::exception {
			public:
				BadTypeException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~BadTypeException() throw();
			
			private:
				std::string		_error;
		};

		class BadValueException : public std::exception {
			public:
				BadValueException(std::string error) throw();
				virtual const char *what() const throw();
				virtual ~BadValueException() throw();
			
			private:
				std::string		_error;
		};

	private:
		Lexer();

		void						getInstruction(std::string &line);
		void						getType(std::string &line);
		void						getValue(std::string &line);
		bool						skipLine(const std::string &line);
		bool						isComment(const std::string &line, size_t pos);
		void						isNumber(const std::string &value) const;
		void						isGoodType();
		void						checkMissing() const;

		std::istream				*_stream;
		bool						_file;
		bool						_endOfCin;
		bool						_hasExit;
		static const std::string	_instructions[];
		static const std::string	_types[]; 
		std::vector<tokensType>		_tokens;
		tokensType					_line;
		std::vector<std::string>	_exceptions;
		int							_lineNbr;
};

#endif
