/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OpFactory.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 15:37:56 by mverdier          #+#    #+#             */
/*   Updated: 2018/01/31 18:37:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OPFACTORY_HPP
#define OPFACTORY_HPP

#include <iostream>
#include <string>
#include <vector>
#include "IOperand.hpp"

class OpFactory {
	public:
		OpFactory();
		virtual ~OpFactory();

		const IOperand	*createOperand(eOperandType type, const std::string &value) const;

	private:
		OpFactory(OpFactory const &src);
		OpFactory& operator=(OpFactory const &rhs);

		const IOperand	*createInt8(const std::string &value) const;
		const IOperand	*createInt16(const std::string &value) const;
		const IOperand	*createInt32(const std::string &value) const;
		const IOperand	*createFloat(const std::string &value) const;
		const IOperand	*createDouble(const std::string &value) const;

		typedef const IOperand *(OpFactory::*FuncType)(const std::string &) const;
		std::vector<FuncType>	_createMethods;
};

#endif
