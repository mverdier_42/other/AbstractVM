# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/09 13:44:17 by mverdier          #+#    #+#              #
#    Updated: 2018/02/21 17:14:07 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

ORANGE =	\033[1;33m   #It is actually Yellow, but i changed yellow to orange.

GREEN =		\033[1;32m

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#

NAME = 		avm

ROOT =		./..

SRCDIR =	./srcs

INCDIR =	./includes

OBJDIR =	./objs

SRC =		main.cpp Lexer.cpp Parser.cpp OpFactory.cpp

INC =		Lexer.hpp Parser.hpp tokensStruct.hpp IOperand.hpp Operand.hpp	\
			OpFactory.hpp MutantStack.hpp

SRCS =		$(SRC:%=$(SRCDIR)/%)

OBJS =		$(SRC:%.cpp=$(OBJDIR)/%.o)

INCS =		$(INC:%=$(INCDIR)/%)

#------------------------------------------------------------------------------#

CC =		clang++

CFLAGS =	-Wall			\
			-Wextra			\
			-Werror			\
			-std=c++11

INCFLAGS =	-I $(INCDIR)

FLAGS =		$(CFLAGS)		\
			$(INCFLAGS)

#------------------------------------------------------------------------------#
# Primary rules

all:
	@$(MAKE) $(NAME)

$(NAME): $(OBJS)
	@$(MAKE) printname
	@printf "%-15s%s\n" Linking $@
	@$(CC) -o $@ $^
	@printf "$(GREEN)"
	@echo "Compilation done !"
	@printf "$(RES)"

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp $(INCS)
	@mkdir -p $(OBJDIR)
	@$(MAKE) printname
	@printf "%-15s%s\n" Compiling $@
	@$(CC) $(FLAGS) -o $@ -c $<

printname:
	@printf "$(ORANGE)"
	@printf "[%-15s " "$(NAME)]"
	@printf "$(RES)"

clean:
	@$(MAKE) printname
	@echo Suppressing obj files
	@printf "$(RED)"
	rm -rf $(OBJS)
	@rm -rf $(OBJDIR)
	@printf "$(RES)"

fclean: clean
	@$(MAKE) printname
	@echo Suppressing $(NAME)
	@printf "$(RED)"
	rm -rf $(NAME)
	@printf "$(RES)"

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

git:	# A rule to make git add easier
	@$(MAKE) printname
	@echo Adding files to git repository
	@printf "$(GREEN)"
	git add $(SRCS) $(INCS) Makefile
	@printf "$(RES)"
	@git status

.PHONY: all clean re fclean git Pony
