touch ./srcs/$1.cpp
touch ./includes/$1.hpp

#hpp

/bin/echo -n "#ifndef " >> ./includes/$1.hpp
echo "$1_HPP" | tr '[:lower:]' '[:upper:]' >> ./includes/$1.hpp
/bin/echo -n "#define ">> ./includes/$1.hpp
echo "$1_HPP" | tr '[:lower:]' '[:upper:]' >> ./includes/$1.hpp

echo "\n#include <iostream>\n" >> ./includes/$1.hpp

echo "class $1 {\n\tpublic:" >> ./includes/$1.hpp
echo "\t\t$1();\n\t\t$1($1 const & src);\n\t\tvirtual ~$1();" >> ./includes/$1.hpp
echo "\n\t\t$1& operator=($1 const& rhs);" >> ./includes/$1.hpp
echo "\n\tprivate:\n\n\tprotected:" >> ./includes/$1.hpp
echo "\n};\n" >> ./includes/$1.hpp

echo "std::ostream& operator<<(std::ostream& os, $1 const& obj);\n" >> ./includes/$1.hpp

echo "#endif" >> ./includes/$1.hpp

#cpp

echo "#include <iostream>" >> ./srcs/$1.cpp
echo "#include \"$1.hpp\"\n" >> ./srcs/$1.cpp
echo "$1::$1() {\n}\n" >> ./srcs/$1.cpp
echo "$1::$1($1 const& src) {\n\t*this = src;\n}\n">> ./srcs/$1.cpp
echo "$1::~$1() {\n}\n" >> ./srcs/$1.cpp
echo "$1& $1::operator=($1 const& rhs) {\n\n\treturn (*this);\n}\n" >> ./srcs/$1.cpp
echo "std::ostream& operator<<(std::ostream& os, $1 const& obj) {" >> ./srcs/$1.cpp
echo "\tos << \"\";\n\n\treturn (os);\n}" >> ./srcs/$1.cpp
