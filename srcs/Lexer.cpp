/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Lexer.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 19:19:48 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/26 19:26:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <cctype>
#include <cstdlib>
#include <limits>
#include <cstring>
#include "Lexer.hpp"

const std::string	Lexer::_instructions[] = { "push", "pop", "dump", "assert", "add", "sub", "mul", "div", "mod", "print", "exit" };
const std::string	Lexer::_types[] = { "int8", "int16", "int32", "float", "double" };

Lexer::Lexer(std::istream &stream) : _stream(&stream), _file (false),
	_endOfCin(false), _hasExit(false), _lineNbr(1) {

}

Lexer::Lexer(Lexer const& src) : _stream(src._stream), _file(src._file),
	_endOfCin(src._endOfCin), _hasExit(src._hasExit), _lineNbr(src._lineNbr) {
	*this = src;
}

Lexer::~Lexer() {
}

Lexer& Lexer::operator=(Lexer const& rhs) {
	_stream = rhs._stream;
	_file = rhs._file;
	_endOfCin = rhs._endOfCin;
	_hasExit = rhs._hasExit;
	_lineNbr = rhs._lineNbr;

	return (*this);
}

void	Lexer::setStream(std::istream &stream) {
	_stream = &stream;
	_file = true;
}

std::vector<tokensType>	&Lexer::getTokens() {
	return (_tokens);
}

std::vector<std::string>	&Lexer::getExceptions() {
	return (_exceptions);
}

void						Lexer::tokenise() {
	std::string		line;

	while (std::getline(*_stream, line)) {
		if (!skipLine(line)) {
			try {
				getInstruction(line);
				getType(line);
				getValue(line);
				_tokens.push_back(_line);
			}
			catch (std::exception & e) {
				_exceptions.push_back(e.what());
			}
		}
		if (_file == false && _endOfCin == true)
			break;
		_lineNbr++;
	}

	try {
		checkMissing();
	}
	catch (std::exception & e) {
		_exceptions.push_back(e.what());
	}
}

void						Lexer::getInstruction(std::string &line) {
	std::string	instruction;
	size_t		pos;

	_line.instruction = "";
	_line.type = "";
	_line.value = "";
	_line.line = std::to_string(_lineNbr);

	if (_hasExit == true)
		throw BadInstructionException("Error : There is instruction(s) after \"exit\"");

	if ((pos = line.find_first_of(' ')) != std::string::npos)
		instruction = line.substr(0, pos);
	else if ((pos = line.find_first_of(';')) != std::string::npos)
		instruction = line.substr(0, pos);
	else
		instruction = line;

	for (size_t i = 0; i < sizeof(_instructions) / sizeof(std::string); i++) {
		if (instruction.compare(_instructions[i]) == 0) {
			_line.instruction = instruction;
			if (!instruction.compare("exit"))
				_hasExit = true;
			return ;
		}
	}

	throw BadInstructionException("Error on line " + _line.line + " : Bad instruction");
}

void						Lexer::getType(std::string &line) {
	size_t		sPos;		// Space position.
	size_t		opPos;		// Opening parenthese.
	std::string	type;
	bool		goodType = false;

	if (_line.instruction.compare("push") != 0 && _line.instruction.compare("assert") != 0) {
		if (!isComment(line, line.find_first_of(' ')) && !isComment(line, line.find_first_of('\t')))
			throw BadTypeException("Error on line " + _line.line + " : " + _line.instruction + " should not have argument");
	}

	if (_line.instruction.empty() ||
		(_line.instruction.compare("push") != 0 && _line.instruction.compare("assert") != 0))
		return ;

	if ((sPos = line.find_first_of(' ')) == std::string::npos)
		throw BadTypeException("Error on line " + _line.line + " : Missign type and value for " + _line.instruction);

	if ((opPos = line.find_first_of('(')) == std::string::npos)
		throw BadValueException("Error on line " + _line.line + " : Missing value for " + _line.instruction);

	sPos++;		// Skip space.
	type = line.substr(sPos, opPos - sPos);
	for (size_t i = 0; i < sizeof(_types) / sizeof(std::string); i++) {
		if (type.compare(_types[i]) == 0)
			goodType = true;
	}
	if (!goodType)
		throw BadTypeException("Error on line " + _line.line + " : Bad type " + type + " for " + _line.instruction);

	_line.type = type;
}

void						Lexer::getValue(std::string &line) {
	size_t		opPos;		// Opening parenthese.
	size_t		cpPos;		// Closing parenthese.
	std::string	value;

	if (_line.instruction.empty() ||
		(_line.instruction.compare("push") != 0 && _line.instruction.compare("assert") != 0))
		return ;

	if ((opPos = line.find_first_of('(')) == std::string::npos)
		throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", missing '('");

	if ((cpPos = line.find_first_of(')')) == std::string::npos)
		throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", missing ')'");

	if (cpPos != line.length() - 1 && !isComment(line, cpPos + 1))
		throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", there is something after ')'");

	opPos++;	// Skip opening parenthese.
	value = line.substr(opPos, cpPos - opPos);
	isNumber(value);
	_line.value = value;
	isGoodType();
}

bool					Lexer::skipLine(const std::string &line) {
	if (line.empty())
		return (true);

	if (line.find(";;") != line.npos && line.length() == 2) {
		_endOfCin = true;
		return (true);
	}

	for (size_t i = 0; i < line.length(); i++) {
		if (line[i] != ' ' && line[i] != ';')
			return (false);
		if (line[i] == ';')
			return (true);
	}

	return (true);
}

bool					Lexer::isComment(const std::string &line, size_t pos) {
	if (pos == std::string::npos)
		return (true);

	for (size_t i = pos; i < line.size(); i++) {
		if (line[i] != ' ' && line[i] != ';')
			return (false);
		if (line[i] == ';')
			return (true);
	}
	return (false);
}

void					Lexer::isNumber(const std::string &value) const {
	bool	pt = false;

	if ((!_line.type.compare("float") || !_line.type.compare("double")) &&
		(!value.compare("-inf") || !value.compare("+inf") || !value.compare("nan")))
		return ;

	if (value.empty())
		throw BadValueException("Error on line " + _line.line + " : There is no value for " + _line.instruction);

	for (size_t i = 0; i < value.length(); i++) {
		if (i == 0 && (value[i] == '-' || value[i] == '+'))
			continue ;
		if ((value[i] < '0' || value[i] > '9') && value[i] != '.')
			throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", value is not a number");
		if (value[i] == '.' && pt == false && i < value.length() - 1)
			pt = true;
		else if (value[i] == '.')
			throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", value has a misplaced or multiple point(s)");
	}
	if (pt == true && _line.type.compare("float") && _line.type.compare("double"))
		throw BadValueException("Error on line " + _line.line + " : Value bad formated for " + _line.instruction + ", non float type value has a floating point");
}

void					Lexer::isGoodType() {
	long double	tmp;

	tmp = strtold(_line.value.c_str(), NULL);

	if (!_line.type.compare("int8")) {
		if (tmp > std::numeric_limits<int8_t>::max())
			throw BadValueException("Error on line " + _line.line + " : Value is bigger than int8 max value");
		if (tmp < std::numeric_limits<int8_t>::min())
			throw BadValueException("Error on line " + _line.line + " : Value is lower than int8 min value");
	}

	if (!_line.type.compare("int16")) {
		if (tmp > std::numeric_limits<int16_t>::max())
			throw BadValueException("Error on line " + _line.line + " : Value is bigger than int16 max value");
		if (tmp < std::numeric_limits<int16_t>::min())
			throw BadValueException("Error on line " + _line.line + " : Value is lower than int16 min value");
	}
	
	if (!_line.type.compare("int32")) {
		if (tmp > std::numeric_limits<int32_t>::max())
			throw BadValueException("Error on line " + _line.line + " : Value is bigger than int32 max value");
		if (tmp < std::numeric_limits<int32_t>::min())
			throw BadValueException("Error on line " + _line.line + " : Value is lower than int32 min value");
	}
	
	if (!_line.type.compare("float")) {
		if (tmp > std::numeric_limits<float>::max() && tmp != std::numeric_limits<float>::infinity())
			throw BadValueException("Error on line " + _line.line + " : Value is bigger than float max value");
		if (tmp < -std::numeric_limits<float>::max() && tmp != -std::numeric_limits<float>::infinity())
			throw BadValueException("Error on line " + _line.line + " : Value is lower than float min value " + std::to_string(tmp) + " / " + std::to_string(std::numeric_limits<float>::min()));
	}
	
	if (!_line.type.compare("double")) {
		if (tmp > std::numeric_limits<double>::max() && tmp != std::numeric_limits<double>::infinity())
			throw BadValueException("Error on line " + _line.line + " : Value is bigger than double max value");
		if (tmp < -std::numeric_limits<double>::max() && tmp != -std::numeric_limits<double>::infinity())
			throw BadValueException("Error on line " + _line.line + " : Value is lower than double min value");
	}
}

void	Lexer::checkMissing() const {
	if (_file == false && _endOfCin == false)
		throw BadInstructionException("Error : Missing \";;\"");
	if (_hasExit == false)
		throw BadInstructionException("Error : Missing exit instruction");
}

Lexer::BadInstructionException::BadInstructionException(std::string error) throw() {
	_error = error;
}

const char			*Lexer::BadInstructionException::what() const throw() {
	return (_error.c_str());
}

Lexer::BadInstructionException::~BadInstructionException() throw() {
}


Lexer::BadTypeException::BadTypeException(std::string error) throw() {
	_error = error;
}

const char			*Lexer::BadTypeException::what() const throw() {
	return (_error.c_str());
}

Lexer::BadTypeException::~BadTypeException() throw() {
}


Lexer::BadValueException::BadValueException(std::string error) throw() {
	_error = error;
}

const char			*Lexer::BadValueException::what() const throw() {
	return (_error.c_str());
}

Lexer::BadValueException::~BadValueException() throw() {
}
