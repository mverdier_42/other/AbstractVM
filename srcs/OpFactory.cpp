/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OpFactory.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/30 15:38:34 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/12 19:21:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "OpFactory.hpp"
#include "Operand.hpp"

OpFactory::OpFactory() {
	_createMethods.push_back(&OpFactory::createInt8);
	_createMethods.push_back(&OpFactory::createInt16);
	_createMethods.push_back(&OpFactory::createInt32);
	_createMethods.push_back(&OpFactory::createFloat);
	_createMethods.push_back(&OpFactory::createDouble);
}

OpFactory::OpFactory(OpFactory const& src) {
	*this = src;
}

OpFactory::~OpFactory() {
}

OpFactory& OpFactory::operator=(OpFactory const& rhs) {
	(void)rhs;
	return (*this);
}

const IOperand	*OpFactory::createOperand(eOperandType type, const std::string &value) const {
	return ((this->*(_createMethods[type]))(value));
}

const IOperand	*OpFactory::createInt8(const std::string &value) const {
	return (new Operand<int8_t>(value, INT8));
}

const IOperand	*OpFactory::createInt16(const std::string &value) const {
	return (new Operand<int16_t>(value, INT16));
}

const IOperand	*OpFactory::createInt32(const std::string &value) const {
	return (new Operand<int32_t>(value, INT32));
}

const IOperand	*OpFactory::createFloat(const std::string &value) const {
	return (new Operand<float>(value, FLOAT));
}

const IOperand	*OpFactory::createDouble(const std::string &value) const {
	return (new Operand<double>(value, DOUBLE));
}
