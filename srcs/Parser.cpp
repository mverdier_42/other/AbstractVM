/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Parser.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 19:04:02 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/26 15:34:06 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Parser.hpp"

const std::string	Parser::_instructions[] = { "push", "pop", "dump", "assert", "add", "sub", "mul", "div", "mod", "print", "exit" };
const std::string	Parser::_types[] = { "int8", "int16", "int32", "float", "double" };

Parser::Parser(MutantStack<const IOperand*> &opStack) : _opStack(opStack) {
	_ops.push_back(&Parser::opPush);
	_ops.push_back(&Parser::opPop);
	_ops.push_back(&Parser::opDump);
	_ops.push_back(&Parser::opAssert);
	_ops.push_back(&Parser::opAdd);
	_ops.push_back(&Parser::opSub);
	_ops.push_back(&Parser::opMul);
	_ops.push_back(&Parser::opDiv);
	_ops.push_back(&Parser::opMod);
	_ops.push_back(&Parser::opPrint);
	_ops.push_back(&Parser::opExit);
}

Parser::Parser(Parser const& src) : _opStack(src._opStack) {
	*this = src;
}

Parser::~Parser() {
}

Parser& Parser::operator=(Parser const& rhs) {
	_tokens = rhs._tokens;
	return (*this);
}

void	Parser::setTokens(std::vector<tokensType> &tokens) {
	_tokens = tokens;
}

std::vector<std::string>	&Parser::getExceptions() {
	return (_exceptions);
}

void			Parser::parse() {
	for (size_t i = 0; i < _tokens.size(); i++) {
		for (size_t j = 0; j < sizeof(_instructions) / sizeof(std::string); j++) {
			if (!_tokens[i].instruction.compare(_instructions[j])) {
				try {
					(this->*(_ops[j]))(_tokens[i]);
				}
				catch (std::exception & e) {
					_exceptions.push_back(e.what());
				}
			}
		}
	}
}

void		Parser::opPush(tokensType &token) {
	_opStack.push(_opFactory.createOperand(getType(token), token.value));
}

void		Parser::opPop(tokensType &token) {
	if (_opStack.size() < 1)
		throw TooSmallStackException("Error on line " + token.line + " : Pop on empty stack");
	_opStack.pop();
}

void		Parser::opDump(tokensType &token) {
	(void)token;

	if (_opStack.size() > 0) {
		for (MutantStack<const IOperand*>::container_type::iterator it = --_opStack.end(); it >= _opStack.begin(); --it) {
			std::cout << (*it)->toString() << std::endl;
		}
	}
}

void		Parser::opAssert(tokensType &token) {
	if (_opStack.size() < 1)
		throw TooSmallStackException("Error on line " + token.line + " : Assert on empty stack");

	const IOperand	*op = _opStack.top();

	if (op->getType() != getType(token) || op->toString().compare(token.value) != 0)
		throw AssertException("Error on line " + token.line + " : Assert is false");
}

void		Parser::opAdd(tokensType &token) {
	if (_opStack.size() < 2)
		throw TooSmallStackException("Error on line " + token.line + " : Add when stack has less than 2 contents");

	const IOperand	*op1 = _opStack.top();
	_opStack.pop();

	const IOperand	*op2 = _opStack.top();
	_opStack.pop();

	const IOperand	*op3 = *op2 + *op1;
	_opStack.push(op3);
}

void		Parser::opSub(tokensType &token) {
	if (_opStack.size() < 2)
		throw TooSmallStackException("Error on line " + token.line + " : Sub when stack has less than 2 contents");

	const IOperand	*op1 = _opStack.top();
	_opStack.pop();

	const IOperand	*op2 = _opStack.top();
	_opStack.pop();

	const IOperand	*op3 = *op2 - *op1;
	_opStack.push(op3);
}

void		Parser::opMul(tokensType &token) {
	if (_opStack.size() < 2)
		throw TooSmallStackException("Error on line " + token.line + " : Mul when stack has less than 2 contents");
	
	const IOperand	*op1 = _opStack.top();
	_opStack.pop();

	const IOperand	*op2 = _opStack.top();
	_opStack.pop();

	const IOperand	*op3 = *op2 * *op1;
	_opStack.push(op3);
}

void		Parser::opDiv(tokensType &token) {
	if (_opStack.size() < 2)
		throw TooSmallStackException("Error on line " + token.line + " : Div when stack has less than 2 contents");

	const IOperand	*op1 = _opStack.top();
	_opStack.pop();

	const IOperand	*op2 = _opStack.top();
	_opStack.pop();

	const IOperand	*op3 = *op2 / *op1;
	_opStack.push(op3);
}

void		Parser::opMod(tokensType &token) {
	if (_opStack.size() < 2)
		throw TooSmallStackException("Error on line " + token.line + " : Mod when stack has less than 2 contents");

	const IOperand	*op1 = _opStack.top();
	_opStack.pop();

	const IOperand	*op2 = _opStack.top();
	_opStack.pop();

	const IOperand	*op3 = *op2 % *op1;
	_opStack.push(op3);
}

void		Parser::opPrint(tokensType &token) {
	if (_opStack.size() < 1)
		throw TooSmallStackException("Error on line " + token.line + " : Print on empty stack");

	const IOperand	*op = _opStack.top();

	if (op->getType() == INT8) {
		char	c = std::atoi(op->toString().c_str());
		std::cout << c << std::endl;
	}
	else
		throw AssertException("Error on line " + token.line + " : Top value is not an 8-bit integer");
}

void		Parser::opExit(tokensType &token) {
	(void)token;
}

eOperandType	Parser::getType(tokensType &token) const {
	if (!token.type.compare("int8"))
		return (INT8);
	else if (!token.type.compare("int16"))
		return (INT16);
	else if (!token.type.compare("int32"))
		return (INT32);
	else if (!token.type.compare("float"))
		return (FLOAT);
	else
		return (DOUBLE);
}

Parser::TooSmallStackException::TooSmallStackException(std::string error) throw() {
	_error = error;
}

const char	*Parser::TooSmallStackException::what() const throw() {
	return (_error.c_str());
}

Parser::TooSmallStackException::~TooSmallStackException() throw() {
}

Parser::AssertException::AssertException(std::string error) throw() {
	_error = error;
}

const char	*Parser::AssertException::what() const throw() {
	return (_error.c_str());
}

Parser::AssertException::~AssertException() throw() {
}

Parser::DivException::DivException(std::string error) throw() {
	_error = error;
}

const char	*Parser::DivException::what() const throw() {
	return (_error.c_str());
}

Parser::DivException::~DivException() throw() {
}
