/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 15:42:31 by mverdier          #+#    #+#             */
/*   Updated: 2018/02/21 18:27:49 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>

#include "Lexer.hpp"
#include "Parser.hpp"
#include "IOperand.hpp"
#include "MutantStack.hpp"

int		main(int ac, char **av) {
	if (ac > 2)
		return (0);
	
	MutantStack<const IOperand*>	opStack;
	Lexer 							lexer(std::cin);
	Parser							parser(opStack);
	std::ifstream					file;
	std::vector<tokensType>			tokens;
	std::vector<std::string>		lexerExceptions;
	std::vector<std::string>		parserExceptions;

	if (ac == 2) {
		file.open(av[1]);
		if (!file.is_open()) {
			std::cout << "Failed to open \"" << av[1] << "\"" << std::endl;
			return (0);
		}
		lexer.setStream(file);
	}

	lexer.tokenise();

	if (file.is_open())
		file.close();

	if (!(lexerExceptions = lexer.getExceptions()).empty()) {
		for (size_t i = 0; i < lexerExceptions.size(); i++) {
			std::cout << lexerExceptions[i] << std::endl;
		}
		return (0);
	}

	parser.setTokens(lexer.getTokens());
	parser.parse();

	if (!(parserExceptions = parser.getExceptions()).empty()) {
		for (size_t i = 0; i < parserExceptions.size(); i++) {
			std::cout << parserExceptions[i] << std::endl;
		}
	}

	return (0);
}
